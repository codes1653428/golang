FROM dutchcoders/transfer.sh:latest

# Set the provider and basedir options
ENV PROVIDER=local
ENV BASEDIR=/tmp/

# Expose the necessary port
EXPOSE 8080

# Start the transfer.sh service
CMD ["transfer.sh"]
